package exampleEleven;

public class Employee {
	private String name;
	private int idNumber;
	private String department;
	private String position;
	
	// Constructor
	public Employee(String name, int id, String dept, String pos) {
		this.name = name;
		idNumber = id;
		department = dept;
		position = pos;
	}
	
	public Employee(String name, int id) {
		this.name = name;
		idNumber = id;
		department = "";
		position = "";
	}
	
	public Employee() {
		name = "";
		idNumber = 0;
		department = "";
		position = "";
	}
	
	// Setters
	public void setName(String name) {
		this.name = name;
	}
	
	public void setID(int id) {
		idNumber = id;
	}
	
	public void setDept(String dept) {
		department = dept;
	}
	
	public void setPos(String pos) {
		position = pos;
	}
	
	// Getters
	public String getName() {
		return name;
	}
	
	public int getID() {
		return idNumber;
	}
	
	public String getDept() {
		return department;
	}
	
	public String getPos() {
		return position;
	}
	
	// Methods
	public String toString() {
		return "Name: " + name + "\n" 
				+ "ID: " + idNumber + "\n"
				+ "Department: " + department + "\n"
				+ "Position: " + position;
	}
}
