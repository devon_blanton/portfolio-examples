package exampleEleven;

// overloaded constructor

public class EmployeeDriver {

	public static void main(String[] args) {
		Employee e1 = new Employee();
		Employee e2 = new Employee("Name", 10);
		Employee e3 = new Employee("Name", 10, "Department", "Position");
		
		System.out.println(e1.toString());
		System.out.println();
		System.out.println(e2.toString());
		System.out.println();
		System.out.println(e3.toString());
		System.out.println();
	}

}
