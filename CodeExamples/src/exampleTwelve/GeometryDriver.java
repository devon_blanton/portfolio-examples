package exampleTwelve;
import java.util.Scanner;

// static class members

public class GeometryDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double radius = 5;
		
		double rectangleWidth = 4;
		double rectangleLength = 6;
		
		double triangleBase = 3;
		double triangleHeight = 9;
		
		System.out.println("Circle radius: " + radius);
		System.out.println("Rectangle width: " + rectangleWidth);
		System.out.println("Rectangle length: " + rectangleLength);
		System.out.println("Triangle base: " + triangleBase);
		System.out.println("Triangle height: " + triangleHeight);
		System.out.println();
		
		int input;
		
		System.out.println("Geometry Calculator");
		System.out.println("1. Calculate the Area of the Circle");
		System.out.println("2. Calculate the Area of the Rectangle");
		System.out.println("3. Calculate the Area of the Triangle");
		System.out.println("4. Quit");
		System.out.println();
		System.out.print("Enter your choice (1-4): ");
		input = keyboard.nextInt();
		keyboard.nextLine();
		
		while(input < 1 || input > 4) {
			System.out.println();
			System.out.println("Invalid input.");
			System.out.println("1. Calculate the Area of the Circle");
			System.out.println("2. Calculate the Area of the Rectangle");
			System.out.println("3. Calculate the Area of the Triangle");
			System.out.println("4. Quit");
			System.out.println();
			System.out.print("Enter your choice (1-4): ");
			input = keyboard.nextInt();
			keyboard.nextLine();
		}
		
		switch(input) {
			case 1:
				System.out.println("Circle's area: " + Geometry.calcCircleArea(radius));
				break;
			case 2:
				System.out.println("Rectangle's area: " + Geometry.calcRectangleArea(rectangleWidth, rectangleLength));
				break;
			case 3:
				System.out.println("Triangle's area: " + Geometry.calcTriangleArea(triangleBase, triangleHeight));
				break;
		}
		
		
		keyboard.close();
	}

}
