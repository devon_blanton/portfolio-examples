package exampleTwelve;

public class Geometry {
	// Methods
	public static double calcCircleArea(double radius) {
		if(radius < 0) {
			System.out.println("The radius cannot be negative");
			return -1;
		}
		return Math.PI * Math.pow(radius, 2);
	}
	
	public static double calcRectangleArea(double width, double length) {
		if(width < 0 || length < 0) {
			System.out.println("Neither number can be negative");
			return -1;
		}
		return width * length;
	}
	
	public static double calcTriangleArea(double base, double height) {
		if(base < 0 || height < 0) {
			System.out.println("Neither number can be negative");
			return -1;
		}
		return base * height * 0.5;
	}
	
	
}
