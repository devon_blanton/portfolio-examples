package exampleSixteen;

public class ProductionWorker extends Employee {
	private int shift;
	private double hourlyRate;
	
	// Constructor
	public ProductionWorker(String name, String employeeNumber, String hireDate, int shift, double hourlyRate) {
		super(name, employeeNumber, hireDate);
		this.shift = shift;
		this.hourlyRate = hourlyRate;
	}

	// Getters
	public int getShift() {
		return shift;
	}

	public double getHourlyRate() {
		return hourlyRate;
	}

	// Setters
	public void setShift(int shift) {
		this.shift = shift;
	}

	public void setHourlyRate(double hourlyRate) {
		this.hourlyRate = hourlyRate;
	}
	
	// Methods
	public String toString() {
		return super.toString() + "\nShift: " + shift + "\nPay Rate: " + String.format("$%,.2f", hourlyRate);
	}
	
	
}
