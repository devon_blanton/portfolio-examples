package exampleSixteen;

public class Employee {
	private String name;
	private String employeeNumber;
	private String hireDate;

	// Constructor
	public Employee(String name, String employeeNumber, String hireDate) {
		this.name = name;
		this.employeeNumber = employeeNumber;
		this.hireDate = hireDate;
	}
	
	// Getters
	public String getName() {
		return name;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public String getHireDate() {
		return hireDate;
	}

	// Setters
	public void setName(String name) {
		this.name = name;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	
	
	// Methods
	public String toString() {
		return "Name: " + name + "\nEmployee Number: " + employeeNumber + "\nHire Date: " + hireDate;
	}
}
