package exampleSixteen;

// extending a parent class

public class ProductionWorkerDriver {

	public static void main(String[] args) {
		ProductionWorker productionWorker = new ProductionWorker("Name", "756-A", "2/19/2018", 1, 15);
		
		System.out.println(productionWorker.toString());
	}

}
