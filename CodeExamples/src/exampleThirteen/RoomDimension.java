package exampleThirteen;

public class RoomDimension {
	private double length;
	private double width;

	// Constructor
	public RoomDimension(double len, double w) {
		length = len;
		width = w;
	}
	
	// Getters
	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}

	// Setters
	public void setLength(double length) {
		this.length = length;
	}

	public void setWidth(double width) {
		this.width = width;
	}
	
	// Methods
	public double getArea() {
		return length * width;
	}
	
	public String toString() {
		return "A room of " + length + " ft. by " + width + " ft. has an area of " + getArea() + " ft. squared. ";
	}
}
