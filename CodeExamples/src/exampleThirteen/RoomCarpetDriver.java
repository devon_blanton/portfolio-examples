package exampleThirteen;
import java.util.Scanner;

// aggregation

public class RoomCarpetDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double width;
		double length;
		double cost;
		RoomDimension rd;
		RoomCarpet carpet;
		
		System.out.print("Enter the width of the room: ");
		width = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Enter the length of the room: ");
		length = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Enter the cost per square foot of the carpet: ");
		cost = keyboard.nextDouble();
		keyboard.nextLine();
		
		rd = new RoomDimension(length, width);
		carpet = new RoomCarpet(rd, cost);
		
		System.out.println(carpet.toString());
		
		
		
		
		keyboard.close();
	}

}
