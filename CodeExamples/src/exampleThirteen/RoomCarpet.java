package exampleThirteen;

public class RoomCarpet {
	private RoomDimension size;
	private double carpetCost;
	
	// Constructor
	public RoomCarpet(RoomDimension dim, double cost) {
		size = dim;
		carpetCost = cost;
	}
	
	// Methods
	public double getTotalCost() {
		return size.getArea() * carpetCost;
	}
	
	public String toString() {
		return size.toString() + "At $" + carpetCost + " per square foot, this carpet would cost $" + getTotalCost() + ".";
	}
}
