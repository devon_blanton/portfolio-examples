package exampleTen;

// overloaded methods

public class AreaDriver {

	public static void main(String[] args) {
		System.out.println("Circle radius 3:");
		System.out.println("Area: " + Area.calcArea(3));
		
		System.out.println();
		System.out.println("Rectangle width 3, length 4");
		System.out.println("Area: " + Area.calcArea(3, 4));
		
		System.out.println();
		System.out.println("Cylinder radius 3, height 5");
		System.out.println("Area: " + Area.calcArea(1, 3, 5));
	}

}
