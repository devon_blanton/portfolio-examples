package exampleTen;

public class Area {
	public static double calcArea(double radius) {
		return Math.PI * Math.pow(radius, 2);
	}
	
	public static double calcArea(double width, double length) {
		return width * length;
	}
	
	public static double calcArea(double PI, double radius, double height) {
		return Math.PI * (Math.pow(radius, 2) * height);
	}
}
