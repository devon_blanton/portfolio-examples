package exampleFourteen;

import java.util.Scanner;

// arrays, handling exceptions, throwing exceptions

public class TestScoresDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int[] scores = new int[10];
		
		for(int i = 0; i < scores.length; ++i) {
			System.out.print("Enter score " + (i+1) + ": ");
			scores[i] = keyboard.nextInt();
			keyboard.nextLine();
		}
		
		
		try {
			TestScores testScores = new TestScores(scores);
			System.out.println("Average score: " + testScores.getAvgTestScore());
		}
		catch(IllegalArgumentException e) {
			System.out.println("Error: One of the entered test scores was invalid.");
		}
		
		keyboard.close();
	}

}
