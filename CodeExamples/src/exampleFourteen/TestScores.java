package exampleFourteen;

public class TestScores {
	int[] scores;
	
	// Constructor
	public TestScores(int[] scores) {
		this.scores = new int[scores.length];
		for(int i = 0; i < this.scores.length; ++i) {
			if(scores[i] < 0 || scores[i] > 100) {
				throw new IllegalArgumentException();
			}
			
			this.scores[i] = scores[i];
		}
	}
	
	
	// Methods
	public double getAvgTestScore() {
		int total = 0;
		
		for(int i = 0; i < scores.length; ++i) {
			total += scores[i];
		}
		
		return (double)total / scores.length;
	}
}
