package exampleSeven;

// do-while

public class DoWhile {

	public static void main(String[] args) {
		int count = 0;
		
		do {
			System.out.println(++count);
		}
		while(count < 100);
	}

}
