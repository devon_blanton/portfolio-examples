package exampleFifteen;
import java.util.Scanner;

// sequential search

public class PayrollDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		Payroll payroll = new Payroll();

		int[] employeeIds = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
		int[] hours = new int[7];
		double[] payRates = new double[7];
		
				
		for(int i = 0; i < employeeIds.length; ++i) {
			System.out.println("Employee ID: " + employeeIds[i]);
			System.out.print("Enter hours worked: ");
			hours[i] = keyboard.nextInt();
			keyboard.nextLine();
			while(hours[i] < 0) {
				System.out.println("Hours cannot be negative.");
				System.out.print("Enter hours worked: ");
				hours[i] = keyboard.nextInt();
				keyboard.nextLine();
			}
			
			System.out.print("Enter hourly pay rate: ");
			payRates[i] = keyboard.nextDouble();
			keyboard.nextLine();
			while(payRates[i] < 6.00) {
				System.out.println("Pay rate must be at least 6.00 per hour.");
				System.out.print("Enter hourly pay rate: ");
				payRates[i] = keyboard.nextDouble();
				keyboard.nextLine();
			}
		}
		
		payroll.setHours(hours);
		payroll.setPayRates(payRates);
		
		for(int i = 0; i < employeeIds.length; ++i) {
			System.out.printf("Employee %d gross pay: $%,.2f\n", employeeIds[i], payroll.getWage(employeeIds[i]));
		}
		
		
		keyboard.close();
	}

}
