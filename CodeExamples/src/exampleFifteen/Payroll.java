package exampleFifteen;

public class Payroll {
	int[] employeeIds = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
	int[] hours = new int[7];
	double[] payRates = new double[7];
	double[] wages = new double[7];
	
	// Methods
	public void setHours(int[] hrs)	{
		for(int i=0;i<hours.length;++i)
		{
			hours[i] = hrs[i];
		}
		calcWages();
	}
	
	public void setPayRates(double[] rates) {
		for(int i = 0; i < payRates.length; ++i) {
			payRates[i] = rates[i];
		}
		calcWages();
	}
	
	public double getWage(int ID) {
		for(int i = 0; i < wages.length; ++i) {
			if(employeeIds[i] == ID) {
				return wages[i];
			}
		}
		
		return -1;
	}
	
	private void calcWages() {
		for(int i = 0; i < wages.length; ++i) {
			wages[i] = hours[i] * payRates[i];
		}
	}
}
