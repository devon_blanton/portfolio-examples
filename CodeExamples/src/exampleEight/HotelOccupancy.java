package exampleEight;
import java.util.Scanner;

// for, running total(totalRooms), printf

public class HotelOccupancy {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int floors;
		int occupiedRooms = 0;
		int totalRooms = 0;
		
		
		System.out.println("How many floors does the hotel have?");
		floors = keyboard.nextInt();
		keyboard.nextLine();
		while(floors < 1) {
			System.out.println("Invalid input.");
			System.out.println("There must be at least 1 floor:");
			floors = keyboard.nextInt();
			keyboard.nextLine();
		}
		
		
		for(int i = 1; i <= floors; ++i) {
			int rooms;
			int occupied;
			System.out.println("How many rooms on floor " + i + "?");
			rooms = keyboard.nextInt();
			keyboard.nextLine();
			while(rooms < 10) {
				System.out.println("There must be at least 10 rooms on a floor.");
				System.out.println("How many rooms on floor " + i + "?");
				rooms = keyboard.nextInt();
				keyboard.nextLine();
			}
			totalRooms += rooms;
			
			System.out.println("How many rooms on floor " + i + " are occupied?");
			occupied = keyboard.nextInt();
			keyboard.nextLine();
			while(occupied > rooms) {
				System.out.println("There cannot be more occupied rooms than rooms total on the floor.");
				System.out.println("How many rooms on floor " + i + " are occupied?");
				occupied = keyboard.nextInt();
				keyboard.nextLine();	
			}
			occupiedRooms += occupied;
		}
		
		System.out.println("The hotel has " + totalRooms + " rooms.");
		System.out.println("Of those, " + occupiedRooms + " are occupied.");
		System.out.printf("The hotel is %.1f%% occupied.", (occupiedRooms / (float)totalRooms) * 100);
		
		
		
		
		
		keyboard.close();
	}

}
