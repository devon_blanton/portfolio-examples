package exampleNine;
import java.util.Scanner;

// sentinal value

public class PayrollDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int ID = 1;
		double grossPay;
		double stateTax;
		double federalTax;
		double FICAWithholdings;
		
		double totalGrossPay = 0;
		double totalStateTax = 0;
		double totalFederalTax = 0;
		double totalFICAWithholdings = 0;
		double totalNetPay = 0;
		
		
		while(ID != 0) {
			System.out.println("Enter employee ID, or 0 to stop:");
			ID = keyboard.nextInt();
			keyboard.nextLine();
			if(ID == 0) {
				break;
			}
			
			System.out.println("Enter employee's gross pay:");
			grossPay = keyboard.nextDouble();
			keyboard.nextLine();
			while(grossPay < 0) {
				System.out.println("Gross pay cannot be negative.");
				System.out.println("Enter employee's gross pay:");
				grossPay = keyboard.nextDouble();
				keyboard.nextLine();
			}
			
			
			System.out.println("Enter employee's state tax:");
			stateTax = keyboard.nextDouble();
			keyboard.nextLine();
			while(stateTax < 0 || stateTax > grossPay) {
				System.out.println("State tax should be between 0 and the gross pay.");
				System.out.println("Enter employee's state tax:");
				stateTax = keyboard.nextDouble();
				keyboard.nextLine();
			}
			
			
			System.out.println("Enter employee's federal tax:");
			federalTax = keyboard.nextDouble();
			keyboard.nextLine();
			while(federalTax < 0 || federalTax > grossPay) {
				System.out.println("Federal tax should be between 0 and the gross pay.");
				System.out.println("Enter employee's federal tax:");
				federalTax = keyboard.nextDouble();
				keyboard.nextLine();
			}
			
			
			System.out.println("Enter employee's FICA Withholdings:");
			FICAWithholdings = keyboard.nextDouble();
			keyboard.nextLine();
			while(FICAWithholdings < 0 || FICAWithholdings > grossPay) {
				System.out.println("FICA Withholdings should be between 0 and the gross pay.");
				System.out.println("Enter employee's FICA Withholdings:");
				FICAWithholdings = keyboard.nextDouble();
				keyboard.nextLine();
			}
			
			
			if((stateTax + federalTax + FICAWithholdings) > grossPay) {
				System.out.println("Error: Tax and withholdings total is higher than gross pay.");
				System.out.println("Please reenter data for this employee.");
				continue;
			}
			
			
			Payroll employee = new Payroll(ID, grossPay, stateTax, federalTax, FICAWithholdings);
			System.out.println("Net Pay: " + employee.calcNetPay());
			totalGrossPay += grossPay;
			totalStateTax += stateTax;
			totalFederalTax += federalTax;
			totalFICAWithholdings += FICAWithholdings;
			totalNetPay += employee.calcNetPay();
		}
		
		
		System.out.println("Total gross pay: " + totalGrossPay);
		System.out.println("Total state tax: " + totalStateTax);
		System.out.println("Total federal tax: " + totalFederalTax);
		System.out.println("Total FICA withholdings: " + totalFICAWithholdings);
		System.out.println("Total net pay: " + totalNetPay);
		
		
		
		keyboard.close();
	}

}
