package exampleNine;

public class Payroll {
	int ID;
	double grossPay;
	double stateTax;
	double federalTax;
	double FICAWithholdings;

	// Constructor
	public Payroll(int ID, double grossPay, double stateTax, double federalTax, double FICAWithholdings) {
		this.ID = ID;
		this.grossPay = grossPay;
		this.stateTax = stateTax;
		this.federalTax = federalTax;
		this.FICAWithholdings = FICAWithholdings;
	}
	
	// Getters
	public int getID() {
		return ID;
	}

	public double getGrossPay() {
		return grossPay;
	}

	public double getStateTax() {
		return stateTax;
	}

	public double getFederalTax() {
		return federalTax;
	}

	public double getFICAWithholdings() {
		return FICAWithholdings;
	}

	// Setters
	public void setID(int iD) {
		ID = iD;
	}

	public void setGrossPay(double grossPay) {
		this.grossPay = grossPay;
	}

	public void setStateTax(double stateTax) {
		this.stateTax = stateTax;
	}

	public void setFederalTax(double federalTax) {
		this.federalTax = federalTax;
	}

	public void setFICAWithholdings(double FICAWithholdings) {
		this.FICAWithholdings = FICAWithholdings;
	}
	
	// Methods
	public double calcNetPay() {
		return grossPay - stateTax - federalTax - FICAWithholdings;
	}
	
}
