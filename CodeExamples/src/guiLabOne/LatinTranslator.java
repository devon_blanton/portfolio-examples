package guiLabOne;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LatinTranslator extends Application{

	Label output;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Button left = new Button("Sinister");
		Button right = new Button("Dexter");
		Button center = new Button("Medium");
		
		left.setOnAction(new ButtonClickHandler());
		right.setOnAction(new ButtonClickHandler());
		center.setOnAction(new ButtonClickHandler());
		
		VBox box1 = new VBox(left, right, center);
		box1.setAlignment(Pos.CENTER);
		box1.setSpacing(10);
		
		
		output = new Label("");
		
		
		
		VBox box2 = new VBox(output);
		box2.setAlignment(Pos.CENTER);
		
		
		
		BorderPane borderPane = new BorderPane();
		
		borderPane.setLeft(box1);
		borderPane.setRight(box2);
		borderPane.setPadding(new Insets(20));
		
		
		Scene scene = new Scene(borderPane,350,200);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	class ButtonClickHandler implements EventHandler<ActionEvent> {

		
		@Override
		public void handle(ActionEvent e) {
			switch(((Button)e.getSource()).getText()) {
				case "Sinister":
					output.setText("Left");
					break;
				case "Dexter":
					output.setText("Right");
					break;
				case "Medium":
					output.setText("Center");
					break;
			}
		}
		
	}
}
