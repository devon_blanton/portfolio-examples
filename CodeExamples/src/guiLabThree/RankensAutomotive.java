package guiLabThree;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class RankensAutomotive extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		final double OIL = 26;
		final double LUBE = 18;
		final double RADIATOR = 30;
		final double TRANSMISSION = 80;
		final double INSPECTION = 15;
		final double MUFFLER = 100;
		final double TIRE = 20;

		// Inputs
		CheckBox checkOil = new CheckBox(String.format("Oil change ($%,.2f)", OIL));
		CheckBox checkLube = new CheckBox(String.format("Lube job ($%,.2f)", LUBE));
		CheckBox checkRadiator = new CheckBox(String.format("Radiator flush ($%,.2f)", RADIATOR));
		CheckBox checkTransmission = new CheckBox(String.format("Transmission flush ($%,.2f)", TRANSMISSION));
		CheckBox checkInspection = new CheckBox(String.format("Inspection ($%,.2f)", INSPECTION));
		CheckBox checkMuffler = new CheckBox(String.format("Muffler replacement ($%,.2f)", MUFFLER));
		CheckBox checkTire = new CheckBox(String.format("Tire rotation ($%,.2f)", TIRE));

		Label nonroutinePartsLabel = new Label("Parts charges: ");
		TextField nonroutinePartsInput = new TextField();

		HBox nonroutinePartsContainer = new HBox(10, nonroutinePartsLabel, nonroutinePartsInput);

		Label nonroutineHoursLabel = new Label("Hours of non-routine services ($20/hr): ");
		TextField nonroutineHoursInput = new TextField();

		HBox nonroutineHoursContainer = new HBox(10, nonroutineHoursLabel, nonroutineHoursInput);

		// Buttons
		Button calcPrice = new Button("Calculate Charges");
		Button exit = new Button("Exit");

		HBox buttons = new HBox(10, calcPrice, exit);

		// Outputs
		Label total = new Label("Expenses incurred: ");
		Label totalOutput = new Label();

		HBox output = new HBox(10, total, totalOutput);

		// Events
		calcPrice.setOnAction(e -> {
			double totalPrice = 0;

			totalPrice += checkOil.isSelected()?OIL:0;
			totalPrice += checkLube.isSelected()?LUBE:0;
			totalPrice += checkRadiator.isSelected()?RADIATOR:0;
			totalPrice += checkTransmission.isSelected()?TRANSMISSION:0;
			totalPrice += checkInspection.isSelected()?INSPECTION:0;
			totalPrice += checkMuffler.isSelected()?MUFFLER:0;
			totalPrice += checkTire.isSelected()?TIRE:0;

			boolean error = false;

			if(!nonroutinePartsInput.getText().equals("")) {
				try {
					totalPrice += Double.parseDouble(nonroutinePartsInput.getText());
					nonroutinePartsInput.setStyle(null);
				}
				catch(NumberFormatException exception) {
					error = true;
					nonroutinePartsInput.setStyle("-fx-background-color: rgb(255,200,200)");
				}
			}

			if(!nonroutineHoursInput.getText().equals("")) {
				try {
				totalPrice += Double.parseDouble(nonroutineHoursInput.getText()) * 20;
				nonroutineHoursInput.setStyle(null);
				}
				catch(NumberFormatException exception) {
					error = true;
					nonroutineHoursInput.setStyle("-fx-background-color: rgb(255,200,200)");
				}
			}

			
			if(!error) {
				totalOutput.setText(String.format("$%,.2f", totalPrice));
			}
			else {
				totalOutput.setText("Invalid input.");
			}
		});

		exit.setOnAction(e -> {
			primaryStage.close();
		});
		
		// Root Node
		VBox vBox = new VBox(20, checkOil, checkLube, checkRadiator, checkTransmission, checkInspection, checkMuffler, checkTire, nonroutinePartsContainer, nonroutineHoursContainer, buttons, output);
		vBox.setPadding(new Insets(20));
		vBox.setAlignment(Pos.TOP_LEFT);
		vBox.setMinWidth(250);

		Scene scene = new Scene(vBox, 450, 500);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Ranken's Automotive Maintenance");
		primaryStage.show();
	}

}
