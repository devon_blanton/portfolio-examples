package exampleFour;
import java.util.Scanner;

// switch

public class RomanNumerals {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int num;
		String romanNumeral;
		
		System.out.println("Enter a number from 1-10.");
		num = keyboard.nextInt();
		
		switch(num) {
			case 1:
				romanNumeral = "I";
				break;
			case 2:
				romanNumeral = "II";
				break;
			case 3:
				romanNumeral = "III";
				break;
			case 4:
				romanNumeral = "IV";
				break;
			case 5:
				romanNumeral = "V";
				break;
			case 6:
				romanNumeral = "VI";
				break;
			case 7:
				romanNumeral = "VII";
				break;
			case 8:
				romanNumeral = "VIII";
				break;
			case 9:
				romanNumeral = "IX";
				break;
			case 10:
				romanNumeral = "X";
				break;
			default:
				romanNumeral = "ERROR";
				break;
		}
		
		System.out.println("Your number as a Roman numeral: " + romanNumeral);
		
		
		keyboard.close();
	}

}
