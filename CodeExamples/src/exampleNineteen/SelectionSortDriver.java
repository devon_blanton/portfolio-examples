package exampleNineteen;

// Selection Sort

public class SelectionSortDriver {

	public static void main(String[] args) {
		int[] nums = {7, 4, 8, 1, 3};
		
		selectionSort(nums);
		
		for(int i = 0; i < nums.length; ++i) {
			System.out.println(nums[i]);
		}
	}
	
	public static void selectionSort(int[] array) {
		int startScan, index, minIndex, minValue;
		for (startScan = 0; startScan < (array.length-1); ++startScan) {
			minIndex = startScan;
			minValue = array[startScan];
			for(index = startScan + 1; index < array.length; ++index) {
				if(array[index] < minValue) {
					minValue = array[index];
					minIndex = index;
				}
			}
				array[minIndex] = array[startScan];
				array[startScan] = minValue;
		}
	}

}
