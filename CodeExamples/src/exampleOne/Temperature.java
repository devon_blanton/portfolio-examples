package exampleOne;

public class Temperature {
	private double ftemp;
	
	// Constructor
	public Temperature(double temp) {
		ftemp = temp;
	}
	
	// Setters
	public void setFahrenheit(double temp) {
		ftemp = temp;
	}
	
	
	// Getters
	public double getFahrenheit() {
		return ftemp;
	}
	
	public double getCelsius() {
		return ((5/9) * (ftemp - 32));
	}
	
	public double getKelvin() {
		return ((5/9) * (ftemp - 32)) + 273;
	}
}
