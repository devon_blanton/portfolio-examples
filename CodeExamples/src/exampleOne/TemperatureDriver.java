package exampleOne;
import java.util.Scanner;

// Import statement, instance fields, instance methods, constructors

public class TemperatureDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		double temp;
		
		System.out.println("Enter a temperature in fahrenheit:");
		temp = keyboard.nextDouble();
		
		Temperature temp1 = new Temperature(temp);
		
		System.out.println();
		System.out.println("Temperature in Celsius: " + temp1.getCelsius());
		System.out.println("Temperature in Kelvin: " + temp1.getKelvin());
		
		keyboard.close();
		
	}

}
