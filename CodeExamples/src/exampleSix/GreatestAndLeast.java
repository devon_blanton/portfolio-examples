package exampleSix;
import java.util.Scanner;

// while

public class GreatestAndLeast {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int highest;
		int lowest;
		int current = 0;
		
		System.out.println("Enter numbers, entering -99 when you are finished.");
		System.out.print("Enter a number: ");
		System.out.println();
		current = keyboard.nextInt();
		keyboard.nextLine();
		highest = current;
		lowest = current;
		
		while(current != -99) {
			if(current < lowest) {
				lowest = current;
			}
			else if(current > highest) {
				highest = current;
			}
			

			System.out.print("Enter a number: ");
			System.out.println();
			current = keyboard.nextInt();
			keyboard.nextLine();
		}
		
		
		System.out.println("Highest number: " + highest);
		System.out.println("Lowest number: " + lowest);
		
		
		
		keyboard.close();
	}

}
