package exampleEighteen;

import java.util.ArrayList;

// ArrayList

public class ArrayListDriver {

	public static void main(String[] args) {
		ArrayList<String> arrayList = new ArrayList<String>();
		
		for(int i = 0; i < 100; ++i) {
			arrayList.add(i + "");
		}
		
		for(int i = 0; i < arrayList.size(); ++i) {
			System.out.println(arrayList.get(i));
		}
	}

}
