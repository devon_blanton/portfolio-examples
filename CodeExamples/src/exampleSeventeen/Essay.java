package exampleSeventeen;

public class Essay extends GradedActivity {
	private double grammar;
	private double spelling;
	private double correctLength;
	private double content;

	// Constructor
	public Essay(double grammar, double spelling, double correctLength, double content) {
		this.grammar = grammar;
		this.spelling = spelling;
		this.correctLength = correctLength;
		this.content = content;
	}
	
	// Getters
	public double getGrammar() {
		return grammar;
	}

	public double getSpelling() {
		return spelling;
	}

	public double getCorrectLength() {
		return correctLength;
	}

	public double getContent() {
		return content;
	}

	public double getScore() {
		return (grammar * 0.3) + (spelling * 0.2) + (correctLength * 0.2) + (content * 0.3);
	}
	// Setters
	public void setGrammar(double grammar) {
		this.grammar = grammar;
	}

	public void setSpelling(double spelling) {
		this.spelling = spelling;
	}

	public void setCorrectLength(double correctLength) {
		this.correctLength = correctLength;
	}

	public void setContent(double content) {
		this.content = content;
	}
	
	
}
