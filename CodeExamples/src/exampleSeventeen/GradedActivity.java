package exampleSeventeen;

public class GradedActivity {
	private double score;
	
	// Setters
	public void setScore(double s) {
		score = s;
	}
	
	// Getters
	public double getScore() {
		return score;
	}
	
	// Methods
	public char getGrade() {
		if(getScore() >= 90) {
			return 'A';
		}
		else if(getScore() >= 80) {
			return 'B';
		}
		else if(getScore() >= 70) {
			return 'C';
		}
		else if(getScore() >= 60) {
			return 'D';
		}
		else {
			return 'F';
		}
	}
}
