package exampleSeventeen;

public class CourseGrades {
	private GradedActivity[] grades = new GradedActivity[4];
	private final int NUM_GRADES = 4;
	
	public CourseGrades() {
		
	}
	
	public void setLab(GradedActivity aLab) {
		grades[0] = aLab;
	}
	
	public void setPassFailExam(PassFailExam aPassFailExam) {
		grades[1] = aPassFailExam;
	}
	
	public void setEssay(Essay anEssay) {
		grades[2] = anEssay;
	}
	
	public void setFinalExam(FinalExam aFinalExam) {
		grades[3] = aFinalExam;
	}
	
	
	// Methods
	public String toString() {
		return "Grades:\n"
				+ "Lab Score: " + grades[0].getGrade() + " " +  grades[0].getScore() + "\n"
				+ "Pass/Fail Exam Score: " + grades[1].getGrade() + " " +  grades[1].getScore() + "\n"
				+ "Essay Score: " + grades[2].getGrade() + " " +  grades[2].getScore() + "\n"
				+ "Final Exam Score: " + grades[3].getGrade() + " " + grades[3].getScore();
	}
}
