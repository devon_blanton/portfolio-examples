package exampleSeventeen;

// polymorphism

public class CourseGradesDriver {

	public static void main(String[] args) {
		CourseGrades courseGrades = new CourseGrades();
		
		GradedActivity lab = new GradedActivity();
		lab.setScore(80);
		courseGrades.setLab(lab);
		
		PassFailExam passFailExam = new PassFailExam(10, 3, 70);
		courseGrades.setPassFailExam(passFailExam);
		
		Essay essay = new Essay(80, 90, 75, 100);
		courseGrades.setEssay(essay);
		
		FinalExam finalExam = new FinalExam(50, 10);
		courseGrades.setFinalExam(finalExam);
		
		System.out.println(courseGrades);
		
	}

}
