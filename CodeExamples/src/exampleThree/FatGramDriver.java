package exampleThree;
import java.util.Scanner;

// if-else

public class FatGramDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		int calories;
		double fatGrams;
		double percentage;

		System.out.println("Enter the number of calories in the food item");
		calories = keyboard.nextInt();
		keyboard.nextLine();

		System.out.println("Enter the number of grams of fat in the food item");
		fatGrams = keyboard.nextDouble();
		keyboard.nextLine();

		FatGram item = new FatGram(calories, fatGrams);
		percentage = item.getPercentageOfCaloriesFromFat();

		if(percentage <= 1) {

			System.out.println("Percentage of calories from fat: " + percentage * 100 + "%");

			if(percentage < 0.30) {
				System.out.println("The food is low in fat");
			}
		} 
		else {
			System.out.println("The numbers are invalid.");
		}

		keyboard.close();
	}

}
