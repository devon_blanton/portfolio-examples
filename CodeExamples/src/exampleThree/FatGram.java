package exampleThree;

public class FatGram {
	private int calories;
	private double fatGrams;
	private final int CALORIES_IN_ONE_GRAM = 9;
	
	// Constructor
	public FatGram(int calories, double fatGrams) {
		this.calories = calories;
		this.fatGrams = fatGrams;
	}
	
	
	// Getters
	public int getCalories() {
		return calories;
	}
	
	public double getFatGrams() {
		return fatGrams;
	}
	
	
	// Setters
	public void setCalories(int calories) {
		this.calories = calories;
	}
	
	public void setFatGrams(double fatGrams) {
		this.fatGrams = fatGrams;
	}
	
	// Methods
	public double getPercentageOfCaloriesFromFat() {
		double caloriesFromFat = fatGrams * CALORIES_IN_ONE_GRAM;
		
		return caloriesFromFat / calories;
	}
}
