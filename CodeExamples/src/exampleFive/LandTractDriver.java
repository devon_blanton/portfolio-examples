package exampleFive;
import java.util.Scanner;

// comparing strings, toString, equals

public class LandTractDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double length;
		double width;
		
		LandTract tract1;
		LandTract tract2;
		
		System.out.print("Enter the length of the first tract of land: ");
		length = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Enter the width of the first tract of land: ");
		width = keyboard.nextDouble();
		keyboard.nextLine();
		
		tract1 = new LandTract(length, width);
		
		System.out.print("Enter the length of the second tract of land: ");
		length = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Enter the width of the second tract of land: ");
		width = keyboard.nextDouble();
		keyboard.nextLine();

		tract2 = new LandTract(length, width);
		
		
		System.out.println(tract1.toString());
		System.out.println(tract2.toString());
		
		if(tract1.equals(tract2)) {
			System.out.println("These two tracts of land have the same area.");
		}
		else {
			System.out.println("These two tracts of land do not have the same area.");
		}
		
		
		
		keyboard.close();
	}

}
