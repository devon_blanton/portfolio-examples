package exampleFive;

public class LandTract {
	private double length;
	private double width;
	
	// Constructor
	public LandTract(double len, double w) {
		length = len;
		width = w;
	}
	
	// Methods
	public double getArea() {
		return length * width;
	}
	
	public boolean equals(LandTract object2) {
		if(getArea() == object2.getArea()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public String toString() {
		return "This land is " + length + " miles by " + width + " miles. its area is " + getArea() + " miles squared.";
	}
}
