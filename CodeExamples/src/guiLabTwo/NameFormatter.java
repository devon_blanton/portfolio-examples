package guiLabTwo;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NameFormatter extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// First Name
		Label firstNameLabel = new Label("First Name: ");
		TextField firstNameInput = new TextField();
		HBox firstName = new HBox(20, firstNameLabel, firstNameInput);
		firstName.setAlignment(Pos.CENTER);
		
		// Middle Name
		Label middleNameLabel = new Label("Middle Name: ");
		TextField middleNameInput = new TextField();
		HBox middleName = new HBox(20, middleNameLabel, middleNameInput);
		middleName.setAlignment(Pos.CENTER);
		
		// Last Name
		Label lastNameLabel = new Label("Last Name: ");
		TextField lastNameInput = new TextField();
		HBox lastName = new HBox(20, lastNameLabel, lastNameInput);
		lastName.setAlignment(Pos.CENTER);
		
		// Title
		Label titleLabel = new Label("Title: ");
		TextField titleInput = new TextField();
		HBox title = new HBox(20, titleLabel, titleInput);
		title.setAlignment(Pos.CENTER);
		
		// Format Buttons
		Button formatOne = new Button("First Format");
		Button formatTwo = new Button("Second Format");
		Button formatThree = new Button("Third Format");
		Button formatFour = new Button("Fourth Format");
		Button formatFive = new Button("Fifth Format");
		Button formatSix = new Button("Sixth Format");
		HBox buttons = new HBox(20, formatOne, formatTwo, formatThree, formatFour, formatFive, formatSix);
		buttons.setAlignment(Pos.CENTER);
		
		// Output Label
		Label output = new Label();
		output.setAlignment(Pos.CENTER);

		// Button Events
		formatOne.setOnAction(e -> {
			output.setText(titleInput.getText() + " " + firstNameInput.getText() + " " + middleNameInput.getText() + " " + lastNameInput.getText());
		});
		formatTwo.setOnAction(e -> {
			output.setText(firstNameInput.getText() + " " + middleNameInput.getText() + " " + lastNameInput.getText());
		});
		formatThree.setOnAction(e -> {
			output.setText(firstNameInput.getText() + " " + lastNameInput.getText());
		});
		formatFour.setOnAction(e -> {
			output.setText(lastNameInput.getText() + ", " + firstNameInput.getText() + " " + middleNameInput.getText() + ", " + titleInput.getText());
		});
		formatFive.setOnAction(e -> {
			output.setText(lastNameInput.getText() + ", " + firstNameInput.getText() + " " + middleNameInput.getText());
		});
		formatSix.setOnAction(e -> {
			output.setText(lastNameInput.getText() + ", " + firstNameInput.getText());
		});
		
		// Root Node
		VBox vBox = new VBox(10, firstName, middleName, lastName, title, buttons, output);
		vBox.setAlignment(Pos.TOP_CENTER);
		vBox.setPadding(new Insets(20));
		
		Scene scene = new Scene(vBox);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Name Formatter");
		primaryStage.show();
	}

}
