package exampleTwo;

// if, if-else-if, this

public class BankCharges {
	private double endingBalance;
	private int checksWritten;
	
	// Getters
	public double getEndingBalance() {
		return endingBalance;
	}
	
	public int getChecksWritten() {
		return checksWritten;
	}
	
	// Setters
	public void setEndingBalance(double endingBalance) {
		this.endingBalance = endingBalance;
	}
	
	public void setChecksWritten(int checksWritten) {
		this.checksWritten = checksWritten;
	}
	
	// Methods
	public double getCharge() {
		double charge = 10;
		
		if(checksWritten < 20) {
			charge += 0.10 * checksWritten;
		}
		else if(checksWritten < 40) {
			charge += 0.08 * checksWritten;
		}
		else if(checksWritten < 60) {
			charge += 0.06 * checksWritten;
		}
		else {
			charge += 0.04 * checksWritten;
		}
		
		if(endingBalance < 400) {
			charge += 15;
		}
		
		return charge;
	}
}
